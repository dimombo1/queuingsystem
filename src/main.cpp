#include <iostream>
#include <QApplication>
#include <QObject>
#include <QThread>
#include <QVector>
#include <QTimer>
#include <QStringList>
#include <QString>
#include <memory>
#include "include/QueuingSystemMainGui.hpp"
#include "include/QueuingSystemAutoGui.hpp"


int main(int argc, char ** argv)
{
  int numberOfProducers = 3;
  int numberOfConsumers = 3;
  int bufSize = 3;
  int poissonMean = 600;
  int totalQueries = 12;
  int processingMean = 1500;
  
  wrk::SystemSettings settings {
    numberOfProducers,
    numberOfConsumers,
    bufSize,
    poissonMean,
    totalQueries,
    processingMean
  };

  QApplication app(argc, argv);

  QString binPath = "wavedrom-cli";

  // auto qs = std::make_shared< wrk::QueuingSystem >(settings);

  wrk::QueuingSystemMainGui system (settings, binPath);

  // wrk::QueuingSystemAutoGui system (qs, settings);

  // wrk::QueuingSystemDynGui system (qs, binPath);

  // std::cout << "Here\n";

  // system.run();

  // system.runSimulation();

  return app.exec();
}
