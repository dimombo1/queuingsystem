#include "include/Parser.hpp"

#include <QPair>
#include <algorithm>
#include <QDebug>

#include <iostream>

wrk::Parser::Parser(const std::shared_ptr< QueuingSystem > qs, const QTime & startOfSystem):
    prev_(startOfSystem.hour(), startOfSystem.minute(), startOfSystem.second(), startOfSystem.msec()),
    numberOfProducers_(qs->producers().size()),
    numberOfConsumers_(qs->consumers().size())
{
    init(qs);
    divide_ = 200;
}

void wrk::Parser::init(const std::shared_ptr< QueuingSystem > qs)
{
    QString initWave = "0";

    int total = 0;
    for (int i = 0; i < numberOfProducers_; ++i, ++total)
    {
        auto signal = QJsonObject({
            QPair("name", QString("P%1").arg(i + 1)),
            QPair("wave", initWave),
            QPair("data", QJsonArray()),
         });
        signals_.append(signal);
    }

    for (int i = 0; i < numberOfConsumers_; ++i, ++total)
    {
        auto signal = QJsonObject({
            QPair("name", QString("C%1").arg(i + 1)),
            QPair("wave", initWave),
            QPair("data", QJsonArray()),
        });
        signals_.append(signal);        
    }

    int bufSize = qs->bufSize();
    for (int i = 0; i < bufSize; ++i, ++total)
    {
        auto signal = QJsonObject({
            QPair("name", QString("B%1").arg(i + 1)),
            QPair("wave", initWave),
            QPair("data", QJsonArray()),
        });
        signals_.append(signal);        
    }

    auto rejected = QJsonObject({
            QPair("name", QString("rejected")),
            QPair("wave", initWave),
            QPair("data", QJsonArray()),
    });
    signals_.append(rejected);

    obj_.insert("signal", signals_);
}

void wrk::Parser::queryGenerated(const Query & query, const QTime & eventTime)
{
    int diff = prev_.msecsTo(eventTime);

    if (diff)
    {
        addNDotsToAllElements(1);
        prev_ = prev_.addMSecs(diff);
    }
    
    int producerId = query.producerId();
    auto signal = signals_[producerId - 1].toObject();
    auto data = signal.value("data").toArray();
    auto wave = signal.value("wave").toString();
    auto name = signal.value("name").toString();

    data.append(QString("%1.%2").arg(query.producerId()).arg(query.queryId()));
    wave.append(QString("20"));

    updateSignals(data, name, wave, producerId - 1);

    obj_.insert("signal", signals_);
}

void wrk::Parser::queryPutIntoBuffer(const Query & query, const QTime & eventTime, int index)
{
    int diff = prev_.msecsTo(eventTime);

    if (diff)
    {
        addNDotsToAllElements(1);
        prev_ = prev_.addMSecs(diff);
    }
    int cellIndex = numberOfProducers_ + numberOfConsumers_ + index;
    auto signal = signals_[cellIndex].toObject();
    auto data = signal.value("data").toArray();
    auto wave = signal.value("wave").toString();
    auto name = signal.value("name").toString();

    data.append(QString("%1.%2").arg(query.producerId()).arg(query.queryId()));
    wave.append(QString("2"));

    updateSignals(data, name, wave, cellIndex);

    obj_.insert("signal", signals_);
}

void wrk::Parser::queryRejected(const Query & query, const QTime & eventTime)
{
    int diff = prev_.msecsTo(eventTime);

    if (diff)
    {
        addNDotsToAllElements(1);
        prev_ = prev_.addMSecs(diff);
    }
    int index = signals_.size() - 1;
    auto signal = signals_[index].toObject();
    auto data = signal.value("data").toArray();
    auto wave = signal.value("wave").toString();
    auto name = signal.value("name").toString();

    data.append(QString("%1.%2").arg(query.producerId()).arg(query.queryId()));
    wave.append(QString("20"));

    updateSignals(data, name, wave, index);

    obj_.insert("signal", signals_);
}

void wrk::Parser::queryLeftBuffer(const Query & query, const QTime & eventTime, int index)
{
    int diff = prev_.msecsTo(eventTime);

    if (diff)
    {
        addNDotsToAllElements(1);
        prev_ = prev_.addMSecs(diff);
    }
    int cellIndex = numberOfProducers_ + numberOfConsumers_ + index;
    auto signal = signals_[cellIndex].toObject();
    auto data = signal.value("data").toArray();
    auto wave = signal.value("wave").toString();
    auto name = signal.value("name").toString();

    if ((cellIndex < signals_.size() - 2))
    {
        auto nextBufCell = signals_[cellIndex + 1].toObject();
        auto nextData = nextBufCell.value("data").toArray();
        auto nextWave = nextBufCell.value("wave").toString();
        int lastDataDisplayed = nextWave.lastIndexOf('2');
        int lastLeftCell = nextWave.lastIndexOf('0');
        if (nextData.isEmpty() || (lastDataDisplayed < lastLeftCell))
        {
            wave.append(QString("0"));
        }
    }

    //можно целиком засунуть в reconfigureBuffer для оптимизации

    updateSignals(data, name, wave, cellIndex);

    reconfigureBuffer(cellIndex);

    obj_.insert("signal", signals_);
}

void wrk::Parser::queryEnteredConsumer(const Query & query, const QTime & eventTime, int index)
{
  int diff = prev_.msecsTo(eventTime);

    if (diff)
    {
        addNDotsToAllElements(1);
        prev_ = prev_.addMSecs(diff);
    }
    int cellIndex = numberOfProducers_ + index;
    auto signal = signals_[cellIndex].toObject();
    auto data = signal.value("data").toArray();
    auto wave = signal.value("wave").toString();
    auto name = signal.value("name").toString();

    data.append(QString("%1.%2").arg(query.producerId()).arg(query.queryId()));
    wave.append(QString("2"));

    updateSignals(data, name, wave, cellIndex);

    obj_.insert("signal", signals_);
}

void wrk::Parser::queryLeftConsumer(const Query & query, const QTime & eventTime, int index)
{
  int diff = prev_.msecsTo(eventTime);

    if (diff)
    {
        addNDotsToAllElements(1);
        prev_ = prev_.addMSecs(diff);
    }
    int cellIndex = numberOfProducers_ + index;
    auto signal = signals_[cellIndex].toObject();
    auto data = signal.value("data").toArray();
    auto wave = signal.value("wave").toString();
    auto name = signal.value("name").toString();

    wave.append(QString("0"));

    updateSignals(data, name, wave, cellIndex);

    obj_.insert("signal", signals_);
}

void wrk::Parser::parse(const Event & event)
{
    switch (event.type_)
    {
    case TypeOfEvent::QueryGenerated:
        queryGenerated(event.query_, event.time_);
        break;
    case TypeOfEvent::QueryPutIntoBuffer:
        queryPutIntoBuffer(event.query_, event.time_, event.index_);
        break;
    case TypeOfEvent::QueryRejected:
        queryRejected(event.query_, event.time_);
        break;
    case TypeOfEvent::QueryLeftBuffer:
        queryLeftBuffer(event.query_, event.time_, event.index_);
        break;
    case TypeOfEvent::QueryEnteredConsumer:
        queryEnteredConsumer(event.query_, event.time_, event.index_);
        break;
    case TypeOfEvent::QueryLeftConsumer:
        queryLeftConsumer(event.query_, event.time_, event.index_);
        break;
    default:
        qDebug() << QString("Unknown event");
        break;
    }
}

void wrk::Parser::addNDotsToAllElements(int numberOfDots)
{
    for (int i = 0; i < signals_.size(); ++i)
    {
        auto signal = signals_[i].toObject();
        auto wave = signal.value("wave").toString();
        auto data = signal.value("data").toArray();
        auto name = signal.value("name").toString();
        QString strToAdd;
        strToAdd = QString(numberOfDots, '.');
        
        wave.append(strToAdd);

        signal = QJsonObject({
            QPair("name", name),
            QPair("wave", wave),
            QPair("data", data)
        });
        signals_.replace(i, signal);

        obj_.insert("signal", signals_);
    }
}

const QJsonObject & wrk::Parser::jsonSignals() const
{
    return obj_;
}

void wrk::Parser::reconfigureBuffer(int startIndex)
{
    int endIndex = signals_.size() - 2;

    for (int i = startIndex; i < endIndex; ++i)
    {
        auto currentCell = signals_[i].toObject();
        auto currentData = currentCell.value("data").toArray();
        auto currentWave = currentCell.value("wave").toString();
        auto currentName = currentCell.value("name").toString();

        auto nextBufCell = signals_[i + 1].toObject();
        auto nextData = nextBufCell.value("data").toArray();
        auto nextWave = nextBufCell.value("wave").toString();
        int lastDataDisplayedOnNext = nextWave.lastIndexOf('2');
        int lastLeftCellOnNext = nextWave.lastIndexOf('0');
        
        if (nextData.isEmpty() || (lastDataDisplayedOnNext < lastLeftCellOnNext))
        {
            currentWave.append(QString("."));
        }
        else
        {
            currentData.append(nextData.last().toString());
            currentWave.append("2");
        }

        auto signal = QJsonObject({
            QPair("name", currentName),
            QPair("wave", currentWave),
            QPair("data", currentData),
        });

        signals_.replace(i, signal);
    }

    obj_.insert("signal", signals_);
}

void wrk::Parser::updateSignals(const QJsonArray & data, const QString & name, const QString & wave, int index)
{
    auto signal = QJsonObject({
        QPair("name", name),
        QPair("wave", wave),
        QPair("data", data),
    });

    addNDotsToAllElements(1);

    signals_.replace(index, signal);
}