#include "include/QueuingSystemAutoGui.hpp"

#include <QScreen>
#include <QString>
#include <QLineEdit>
#include <QLabel>

#include <iostream>

wrk::QueuingSystemAutoGui::QueuingSystemAutoGui(std::shared_ptr< QueuingSystem > qs, const SystemSettings & settings):
    qs_(qs),
    isReady_(false),
    namesOfCharachteristics_(QStringList{
        "objects",
        "average time in system",   
        "average time processing query",
        "average time awaiting query",
        "variance of time awaiting query",
        "variance of time processing query",
        "probability of rejection",
        "total generated",
        "efficiency"
    })
{
    QObject::connect(qs_.get(), &QueuingSystem::modelingFinished, this, &QueuingSystemAutoGui::setReady);
    configureTable(settings);
}

void wrk::QueuingSystemAutoGui::run()
{
    qs_->run();
}

void wrk::QueuingSystemAutoGui::setReady()
{
    isReady_ = true;
    processResults();
    table_.show();
}

void wrk::QueuingSystemAutoGui::processResults()
{
    fillTableWithAverageTimeInSystem();
    fillTableWithAverageTimeProcessing();
    fillTableWithAverageTimeAwaitingQuery(); 
    fillTableWithVarianceOfTimeAwaitingQuery();
    fillTableWithVarianceOfTimeProcessing(); // можно задавать границы для случайных чисел для консумеров
    fillTableWithProbabilityOfRejection();
    fillTableWithTotalGenerated();
    fillTableWithEfficiency();
}

void wrk::QueuingSystemAutoGui::configureTable(const SystemSettings & settings)
{
    int numberOfColumns = qs_->numberOfCharachteristics() + 1;
    table_.setColumnCount(numberOfColumns);
    int numberOfRows = settings.numberOfConsumers_ + settings.numberOfProducers_;
    table_.setRowCount(numberOfRows);
    table_.setHorizontalHeaderLabels(namesOfCharachteristics_);
    for (int i = 1; i < numberOfColumns - 1; ++i)
    {
        table_.setColumnWidth(i, 235);
    }

    for (int i = 0; i < settings.numberOfProducers_; ++i)
    {
        auto item = std::make_shared< QTableWidgetItem >(QString("producer %1").arg(i + 1));
        items_.push_back(item);
        table_.setItem(i, 0, item.get());
    }

    int maxRow = settings.numberOfConsumers_ + settings.numberOfProducers_;

    for (int i = settings.numberOfProducers_; i < maxRow; ++i)
    {
        auto item = std::make_shared< QTableWidgetItem >(QString("consumer %1").arg(i - settings.numberOfProducers_ + 1));
        items_.push_back(item);
        table_.setItem(i, 0, item.get());
    }

    table_.resize(table_.screen()->availableGeometry().size() / 2);  
}

void wrk::QueuingSystemAutoGui::fillTableWithAverageTimeInSystem()
{
    fillTableWithProducerData(qs_->averageTimeInSystem(), 1);
}

void wrk::QueuingSystemAutoGui::fillTableWithAverageTimeProcessing()
{
    fillTableWithProducerData(qs_->averageTimeProcessing(), 2);
}

void wrk::QueuingSystemAutoGui::fillTableWithAverageTimeAwaitingQuery()
{
    fillTableWithProducerData(qs_->averageTimeAwaitingQuery(), 3);
}

void wrk::QueuingSystemAutoGui::fillTableWithVarianceOfTimeAwaitingQuery()
{
    fillTableWithProducerData(qs_->varianceTimeAwaitingQuery(), 4);
}

void wrk::QueuingSystemAutoGui::fillTableWithVarianceOfTimeProcessing()
{
    fillTableWithProducerData(qs_->varianceTimeProcessing(), 5);
}

void wrk::QueuingSystemAutoGui::fillTableWithProbabilityOfRejection()
{
    fillTableWithProducerData(qs_->probabilityOfRejection(), 6);
}

void wrk::QueuingSystemAutoGui::fillTableWithTotalGenerated()
{
    fillTableWithProducerData< int >(qs_->totalGenerated(), 7);
}

void wrk::QueuingSystemAutoGui::fillTableWithEfficiency()
{
    fillTableWithConsumerData(qs_->efficiency(), 8);
}


void wrk::QueuingSystemAutoGui::fillTableWithConsumerData(
  const QMap< std::shared_ptr< wrk::Consumer>, double > & data, int columnIndex
)
{
    int numberOfProducers = qs_->producers().size();
    int i = 0;
    while (i < numberOfProducers)
    {
      auto item = std::make_shared< QTableWidgetItem >(QString("-"));
      items_.push_back(item);
      table_.setItem(i++, columnIndex, item.get()); 
    }

    auto consumers = qs_->consumers();

    for (auto && consumer: consumers)
    {
      double probability = data[consumer];
      auto item = std::make_shared< QTableWidgetItem >(QString("%1").arg(probability));
      items_.push_back(item);
      table_.setItem(i++, columnIndex, item.get());   
    }

     table_.repaint();
}