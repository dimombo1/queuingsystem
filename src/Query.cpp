#include "include/Query.hpp"

wrk::Query::Query(int producerId, int queryId):
  producerId_(producerId),
  queryId_(queryId),
  generationTime_(QTime::currentTime())
{}

int wrk::Query::producerId() const
{
  return producerId_;
}

int wrk::Query::queryId() const
{
  return queryId_;
}

bool wrk::Query::operator==(const Query & rhs) const
{
  return (producerId_ == rhs.producerId_) && (queryId_ == rhs.queryId_);
}

bool wrk::compare(const Query & lhs, const Query & rhs)
{
  if (lhs.producerId() != rhs.producerId())
  {
    return lhs.producerId() < rhs.producerId();
  }
  return lhs.queryId() < rhs.queryId();
}

const QTime & wrk::Query::generationTime() const
{
  return generationTime_;
}
