#include "include/QueuingSystemMainGui.hpp"

wrk::QueuingSystemMainGui::QueuingSystemMainGui(const SystemSettings & settings, const QString & binPath):
    qs_(std::make_shared< QueuingSystem >(settings)),
    auto_(std::make_shared< QueuingSystemAutoGui >(qs_, settings)),
    dyn_(std::make_shared< QueuingSystemDynGui >(qs_, binPath)),
    mainLayout_(this),
    autoMode_("auto mode"),
    dynMode_("step mode"),
    startButton_("start simulation"),
    settings_(settings),
    binPath_(binPath)
{
    addRow("number of producers");
    addRow("number of consumers");
    addRow("size of buffer");
    addRow("poisson mean");
    addRow("total queries");
    addRow("processing mean");

    auto label = std::make_shared< QLabel >("result display:");
    modes_.addWidget(label.get());
    autoMode_.setChecked(true);
    modes_.addWidget(&autoMode_);
    modes_.addWidget(&dynMode_);
    labels_.append(label);

    mainLayout_.addLayout(&params_);
    mainLayout_.addLayout(&modes_);

    mainLayout_.addWidget(&startButton_);

    QObject::connect(&startButton_, &QPushButton::clicked, this, &QueuingSystemMainGui::runSimulation);

    show();
}

void wrk::QueuingSystemMainGui::runSimulation()
{
    getSettings();
    qs_ = std::make_shared< QueuingSystem >(settings_);
    if (autoMode_.isChecked())
    {
        auto_ = std::make_shared< QueuingSystemAutoGui >(qs_, settings_);
        dyn_.reset();
    }
    else
    {
        auto_.reset();
        dyn_ = std::make_shared< QueuingSystemDynGui >(qs_, binPath_);
    }
    qDebug() << "simulation run...";
    qs_->run();
}

void wrk::QueuingSystemMainGui::addRow(const QString & name)
{
    auto label = std::make_shared< QLabel >(name);
    auto box = std::make_shared< QSpinBox >();
    box->setMaximum(10000);
    params_.addRow(label.get(), box.get());

    spinBoxes_.append(box);
    labels_.append(label);
}

int wrk::QueuingSystemMainGui::numberOfParams()
{
    return 5;
}

void wrk::QueuingSystemMainGui::getSettings()
{
    int numberOfProducers = spinBoxes_[0]->value();
    int numberOfConsumers = spinBoxes_[1]->value();
    int sizeOfBuffer = spinBoxes_[2]->value();
    int poissonMean = spinBoxes_[3]->value();
    int totalQueries = spinBoxes_[4]->value();
    int processingMean = spinBoxes_[5]->value();

    settings_ = {
        numberOfProducers,
        numberOfConsumers,
        sizeOfBuffer,
        poissonMean,
        totalQueries,
        processingMean
    };
}
