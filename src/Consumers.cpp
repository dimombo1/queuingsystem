#include "include/Consumers.hpp"
#include <algorithm>
#include <iostream>

wrk::Consumers::Consumers(int numberOfConsumers, int processingMean):
    consumers_(QVector< std::shared_ptr < Consumer > >())
{
     consumers_.reserve(numberOfConsumers);

    for (int i = 1; i <= numberOfConsumers; ++i)
    {
       auto consumer = std::make_shared< Consumer >(i, processingMean);
       consumers_.push_back(consumer);
       QObject::connect(consumers_[i - 1].get(), &wrk::Consumer::isReadyToReceiveQuery, this, &wrk::Consumers::getQueryFromBuffer);
    }
}

const QVector< std::shared_ptr< wrk::Consumer > > & wrk::Consumers::consumers() const
{
    return consumers_;
}

void wrk::Consumers::getQueryFromBuffer()
{
    auto freeConsumer = std::find_if(
        std::begin(consumers_),
        std::end(consumers_),
        [] (std::shared_ptr< Consumer > c) {
            return !c->isBusy();
        }
    );

    if (freeConsumer == consumers_.end())
    {
        emit allConsumersAreBusy();
    }
    else
    {
        emit isReadyToReceiveQuery();
    }
}

void wrk::Consumers::receiveQuery(Query query)
{
    auto freeConsumer = std::find_if(
        std::begin(consumers_),
        std::end(consumers_),
        [] (std::shared_ptr< Consumer > c) {
            return !c->isBusy();
        }
    );
    freeConsumer->get()->setBusy(query);
}

void wrk::Consumers::stop()
{
    std::for_each(
        std::begin(consumers_),
        std::end(consumers_),
        [] (std::shared_ptr< Consumer > c) {
            c->stop();
        }
    );
}

void wrk::Consumers::resume()
{
    std::for_each(
        std::begin(consumers_),
        std::end(consumers_),
        [] (std::shared_ptr< Consumer > c) {
            c->resume();
        }
    );
}