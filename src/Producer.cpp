#include "include/Producer.hpp"
#include <chrono>
#include <cmath>

#include <iostream>

wrk::Producer::Producer(int id, int maxQueries, int poissonMean):
  id_(id),
  generatedTotal_(0),
  rejectedTotal_(0),
  poissonMean_(poissonMean),
  totalTimeAwaitingForQuery_(0),
  maxQueries_(maxQueries),
  remainingTime_(0),
  timer_(QTimer()),
  generator_(std::chrono::system_clock::now().time_since_epoch().count() + id * 1000000),
  distr_(poissonMean)
{
  QObject::connect(&timer_, &QTimer::timeout, this, &wrk::Producer::generateQuery);
}

void wrk::Producer::run()
{
  int random = distr_(generator_);
  timer_.setInterval(random);
  timer_.start();
}

void wrk::Producer::stop()
{
  if (timer_.isActive())
  {
    remainingTime_ = timer_.remainingTime();
  }
  timer_.stop();
}

void wrk::Producer::resume()
{
  timer_.start(remainingTime_);
}

void wrk::Producer::generateQuery()
{
  static int totalGenerateAllProducers_ = 0;
  timer_.stop();
  if (totalGenerateAllProducers_ >= maxQueries_)
  {
    emit tooMuchQueries();
    return;
  }
  Query query (id_, ++generatedTotal_);
  ++totalGenerateAllProducers_;
  int random = distr_(generator_);

  totalTimeAwaitingForQuery_ += random; // избыточно
  
  deltas_[random] = (deltas_.find(random) == deltas_.end()) ? 1 : (deltas_[random] + 1);
  timer_.setInterval(random);

  emit queryGenerated (query);
  timer_.start();
}

int wrk::Producer::id() const
{
  return id_;
}

int wrk::Producer::generatedTotal() const
{
  return generatedTotal_;
}

int wrk::Producer::rejectedTotal() const
{
  return rejectedTotal_;
}

void wrk::Producer::increaseNumberOfRejected()
{
  ++rejectedTotal_;
}

double wrk::Producer::probabilityOfRejection() const
{
  return rejectedTotal_ * 1.0 / generatedTotal_;
}

double wrk::Producer::averageTimeAwaitingQuery() const
{
  return totalTimeAwaitingForQuery_ * 1.0 / generatedTotal_;
}

double wrk::Producer::varianceOfTimeAwaitingQuery(int totalSimultaionTime) const
{
  double variance = 0;
  auto distr = distribution();
  double average = averageTimeAwaitingQuery();
  for (auto i = distr.begin(); i != distr.end(); ++i)
  {
    variance += std::pow((i.key() - average), 2) * i.value();
  }
  return variance;
}

QMap< int, double > wrk::Producer::distribution() const
{
  if (!generatedTotal_)
  {
    return {};
  }
  QMap< int, double > returnable;

  for (auto i = deltas_.begin(); i != deltas_.end(); ++i)
  {
    returnable.insert(i.key(), i.value() * 1.0 / generatedTotal_);
  }

  return returnable;
}