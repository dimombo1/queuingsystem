#include "include/QueuingSystem.hpp"

#include<QCoreApplication>
#include<QTime>
#include <algorithm>

#include <iostream>

wrk::QueuingSystem::QueuingSystem(const SystemSettings & settings):
  producers_(Producers()),
  buffer_(Buffer(settings.bufferSize_)),
  consumers_(Consumers(settings.numberOfConsumers_, settings.processingMean_)),
  totalQueries_(0),
  maxQueries_(settings.totalQueries_),
  totalRejected_(0)
{
  QObject::connect(&buffer_, &wrk::Buffer::queryPutIntoBuffer, &consumers_, &wrk::Consumers::getQueryFromBuffer);
  QObject::connect(&consumers_, &wrk::Consumers::isReadyToReceiveQuery, &buffer_, &wrk::Buffer::sendQueryToConsumer);
  QObject::connect(&buffer_, &wrk::Buffer::querySentToConsumer, &consumers_, &wrk::Consumers::receiveQuery);
  QObject::connect(&buffer_, &wrk::Buffer::queryRejected, this, &wrk::QueuingSystem::increaseNumberOfRejected);
  
  auto consumers = consumers_.consumers();

  for (int i = 0; i < consumers.size(); ++i)
  {
    QObject::connect(consumers[i].get(), &Consumer::endProcessingQuery, this, &QueuingSystem::queryLeftConsumer);
  }
 
  producers_.reserve(settings.numberOfProducers_);

  for (int i = 1; i <= settings.numberOfProducers_; ++i)
  {
      auto producer = std::make_shared< Producer >(i, settings.totalQueries_, settings.poissonMean_);
      producers_.push_back(producer);

      QObject::connect(producers_[i - 1].get(), &wrk::Producer::queryGenerated, &buffer_, &wrk::Buffer::receiveQueryFromProducer); 
  }
}

void wrk::QueuingSystem::run()
{
  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [&] (std::shared_ptr< Producer > p) {
      p->run();
    }
  );

  startOfSimulation_ = QTime::currentTime();
}

QMap< std::shared_ptr< wrk::Producer >, double > wrk::QueuingSystem::averageTimeInSystem() const
{
  QMap< std::shared_ptr< wrk::Producer >, double > returnable;

  for (auto && producer: producers_)
  {

    if (!producer->generatedTotal())
    {
      returnable.insert(producer, 0);
      continue;
    }

    double average = totalTimeInSystem_[producer] * 1.0 / producer->generatedTotal();
    returnable.insert(producer, average);
  }

  return returnable;
}

QMap< std::shared_ptr< wrk::Producer >, double > wrk::QueuingSystem::averageTimeProcessing() const
{
  QMap< std::shared_ptr< Producer >, double > returnable;

  for (auto && producer: producers_)
  {
    if (!producer->generatedTotal())
    {
      returnable.insert(producer, 0);
      continue;
    }

    double average = totalTimeProcessing_[producer] * 1.0 / producer->generatedTotal();
    returnable.insert(producer, average);
  }

  return returnable;
}

QMap< std::shared_ptr< wrk::Producer >, double > wrk::QueuingSystem::averageTimeAwaitingQuery() const
{
  QMap< std::shared_ptr< Producer >, double > returnable;

  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [&] (std::shared_ptr< Producer > producer) {
      returnable.insert(producer, producer->averageTimeAwaitingQuery());
    }
  );
  return returnable;
}

QMap< std::shared_ptr< wrk::Producer >, double > wrk::QueuingSystem::probabilityOfRejection() const
{
  QMap< std::shared_ptr< Producer >, double > returnable;
  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [&returnable] (std::shared_ptr< Producer > producer) {
      returnable.insert(producer, producer->probabilityOfRejection());
    }
  );
  return returnable;
}

QMap< std::shared_ptr< wrk::Producer >, double > wrk::QueuingSystem::varianceTimeAwaitingQuery() const
{
  QMap< std::shared_ptr< Producer >, double > returnable;
  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [&] (std::shared_ptr< Producer > producer) {
      int simulationTime = startOfSimulation_.msecsTo(QTime::currentTime());
      returnable.insert(producer, producer->varianceOfTimeAwaitingQuery(simulationTime));
    }
  );
  return returnable;
}

QMap< std::shared_ptr< wrk::Producer >, double > wrk::QueuingSystem::varianceTimeProcessing() const
{
  QMap< std::shared_ptr< wrk::Producer >, double > returnable;
  auto distribution = distributionProcessing();
  auto average = averageTimeProcessing();

  // std::cout << "varianceTimeProcessing:\n";

  for (auto && producer: producers_)
  {
    double variance = 0;
    double avg = average[producer];
    auto distr = distribution[producer];

    for (auto delta = distr.begin(); delta != distr.end(); ++delta)
    {
      variance += (std::pow((delta.key() - avg), 2) * delta.value());
    }
    returnable.insert(producer, variance);
  }
  return returnable;
}

void wrk::QueuingSystem::queryLeftConsumer(Query query, int processingTime, int id)
{
  ++totalQueries_;
  
  auto producer = std::find_if(
    std::begin(producers_),
    std::end(producers_),
    [&query] (std::shared_ptr< Producer > p) {
      return p->id() == query.producerId();
    }
  );

  QTime now = QTime::currentTime();
  int timeInSystem = now.msecsSinceStartOfDay() - query.generationTime().msecsSinceStartOfDay();
  if (totalTimeInSystem_.find(*producer) == totalTimeInSystem_.end())
  {
    totalTimeInSystem_[*producer] = (timeInSystem);
    totalTimeProcessing_[*producer] = processingTime;
    auto delta = deltasProcessing_[*producer];
    if  (delta.find(processingTime) == delta.end())
    {
      deltasProcessing_[*producer][processingTime] = 1;
    }
    else
    {
      deltasProcessing_[*producer][processingTime] += 1;
    }
  }
  else
  {
    totalTimeInSystem_[*producer] += (timeInSystem);
    totalTimeProcessing_[*producer] += processingTime;
    auto delta = deltasProcessing_[*producer];
    if  (delta.find(processingTime) == delta.end())
    {
      deltasProcessing_[*producer][processingTime] = 1;
    }
    else
    {
      deltasProcessing_[*producer][processingTime] += 1;
    }
  }
  
  if (totalQueries_ == maxQueries_ - totalRejected_)
  {
    emit modelingFinished();
  }
}

QMap< std::shared_ptr< wrk::Producer >, QMap< int, double > > wrk::QueuingSystem::distributionProcessing() const
{
  QMap< std::shared_ptr< Producer >, QMap< int, double > > returnable;
  for (auto deltas = deltasProcessing_.begin(); deltas != deltasProcessing_.end(); ++deltas)
  {
    QMap< int, double > distribution;
    for (auto i = deltas.value().begin(); i != deltas.value().end(); ++i)
    {
      int processedTotal = deltas.key()->generatedTotal() - deltas.key()->rejectedTotal();
      distribution.insert(i.key(), i.value() * 1.0 / processedTotal);
    }
    returnable.insert(deltas.key(), distribution);
  }

  return returnable;
}

int wrk::QueuingSystem::numberOfCharachteristics() const
{
  return 8; // количество характеристик, которые подсчитывает система
}

int wrk::QueuingSystem::bufSize() const
{
  return buffer_.capacity();
}

QMap< std::shared_ptr< wrk::Consumer >, double > wrk::QueuingSystem::efficiency() const
{
  QMap< std::shared_ptr< wrk::Consumer >, double > returnable;
  auto consumers = consumers_.consumers();

  for (auto && consumer: consumers)
  {
    int simulationTime = startOfSimulation_.msecsTo(QTime::currentTime());
    returnable.insert(consumer, consumer->efficiency(simulationTime));
  }
  return returnable;
}

QMap< std::shared_ptr< wrk::Producer >, int > wrk::QueuingSystem::totalGenerated() const
{
  QMap< std::shared_ptr< Producer >, int > returnable;
  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [&returnable] (std::shared_ptr< Producer > producer) {
      returnable.insert(producer, producer->generatedTotal());
    }
  );
  return returnable;
}

const wrk::QueuingSystem::Producers & wrk::QueuingSystem::producers() const
{
  return producers_;
}

const QVector< std::shared_ptr< wrk::Consumer > > & wrk::QueuingSystem::consumers() const
{
  return consumers_.consumers();
}

const wrk::Buffer & wrk::QueuingSystem::buffer() const
{
  return buffer_;
}

void wrk::QueuingSystem::increaseNumberOfRejected(Query query)
{
  ++totalRejected_;
  auto iter = std::find_if(
    std::begin(producers_),
    std::end(producers_),
    [&query] (std::shared_ptr< Producer > p) {
      return (p->id() == query.producerId());
    }
  );

  (*iter)->increaseNumberOfRejected();
}

void wrk::QueuingSystem::stop()
{
  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [] (std::shared_ptr< Producer > p) {
      p->stop();
    }
  );

  consumers_.stop();
}

void wrk::QueuingSystem::resume()
{
  std::for_each(
    std::begin(producers_),
    std::end(producers_),
    [] (std::shared_ptr< Producer > p) {
      p->resume();
    }
  );

  consumers_.resume();
}