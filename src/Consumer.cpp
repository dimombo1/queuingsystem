#include "include/Consumer.hpp"
#include <QThread>
#include <random>

#include <iostream>

wrk::Consumer::Consumer(int id, int processingMean):
  id_(id),
  totalTimeProcessing_(0),
  isBusy_(false),
  processed_(QVector< Query >()),
  timer_(QTimer()),
  remainingTime_(0),
  gen_(rd_()),
  distr_(processingMean - 500, processingMean + 500)
{
  timer_.setSingleShot(true);
  QObject::connect(&timer_, &QTimer::timeout, this, &wrk::Consumer::setFree);
}

bool wrk::Consumer::isBusy() const
{
  return isBusy_;
}

int wrk::Consumer::id() const
{
  return id_;
}

void wrk::Consumer::setBusy(Query query)
{
  isBusy_ = true;
  int random = distr_(gen_);
  totalTimeProcessing_ += random;

  timer_.setInterval(random);
  processed_.push_back(query);
  timer_.start();
  emit startProcessingQuery(query, id_ - 1);
}

void wrk::Consumer::getQueryFromBuf()
{
  if (!isBusy_)
  {
    emit isReadyToReceiveQuery();
  }
}

void wrk::Consumer::receiveQuery(Query query)
{
  setBusy(query);
}

void wrk::Consumer::stop()
{
  if (timer_.isActive())
  {
    remainingTime_ = timer_.remainingTime();
  }
  timer_.stop();
}

void wrk::Consumer::resume()
{
  timer_.start(remainingTime_);
}

void wrk::Consumer::setFree()
{
  isBusy_ = false;
  auto query = processed_.back();
  emit endProcessingQuery(processed_.back(), timer_.interval(), id_ - 1);
  emit isReadyToReceiveQuery();
}

const QVector< wrk::Query > & wrk::Consumer::processed() const
{
  return processed_;
}

int wrk::Consumer::totalTimeProcessing() const
{
  return totalTimeProcessing_;
}

double wrk::Consumer::efficiency(int totalSimulationTime) const
{

  double returnable = totalTimeProcessing_ * 1.0 / totalSimulationTime;
  return (returnable >= 1) ? 1 : returnable;
}