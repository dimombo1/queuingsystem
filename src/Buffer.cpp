#include "include/Buffer.hpp"
#include <QVector> 
#include <algorithm>
#include <iterator>

#include <iostream>

wrk::Buffer::Buffer(int capacity):
  capacity_(capacity),
  queries_(QList< Query >())
{}

int wrk::Buffer::capacity() const
{
  return capacity_;
}

const QList< wrk::Query > & wrk::Buffer::queries() const
{
  return queries_;
}

void wrk::Buffer::receiveQueryFromProducer(Query query)
{
  if (queries_.size() == capacity_)
  {
    emit queryRejected(query);
    return;
  }

  queries_.push_back(query);
  int cellIndex = queries_.size() - 1;
  emit queryPutIntoBuffer(query, cellIndex);
}

void wrk::Buffer::sendQueryToConsumer()
{
  static int totalSent = 0;
  if (queries_.isEmpty())
  {
    return;
  }
  QVector< Query > temp;
  temp.reserve(queries_.size());

  std::copy(
    queries_.begin(),
    queries_.end(),
    std::back_inserter(temp)
  );

  std::sort(
    temp.begin(),
    temp.end(),
    wrk::compare
  );

  auto priorityQuery = temp.first();

  auto queryIter = std::find(
    queries_.begin(),
    queries_.end(),
    priorityQuery
  );

  Query queryToSend = *queryIter;

  int index = getIndex(queryIter);
  queries_.erase(queryIter);
  emit queryLeftBuffer(queryToSend, index);
  emit querySentToConsumer(queryToSend);
}

int wrk::Buffer::getIndex(QList< Query >::iterator iter)
{
  int index = 0;
  for (auto i = queries_.begin(); i != iter; ++i, ++index)
  {}
  return index;
}