#include "include/QueuingSystemDynGui.hpp"

#include <QJsonDocument>
#include <QFile>
#include <QDebug>
#include <QProcess>
#include <cstdlib>

#include <iostream>

wrk::QueuingSystemDynGui::QueuingSystemDynGui(std::shared_ptr< QueuingSystem > qs, const QString & binPath):
    qs_(qs),
    parser_(qs_, QTime::currentTime()),
    nextButton_("next"),
    scrollArea_(this),
    bin_(binPath),
    jsonFilename_("data/signals.json"),
    imageFilename_("data/signals.png"),
    isReady_(false)
{
    auto producers = qs_->producers();

    for (auto && producer: producers)
    {
        QObject::connect(producer.get(), &wrk::Producer::queryGenerated, this, &QueuingSystemDynGui::queryGenerated);
    }

    auto consumers = qs_->consumers();

    for (auto && consumer: consumers)
    {
        QObject::connect(consumer.get(), &wrk::Consumer::startProcessingQuery, this, &QueuingSystemDynGui::queryEnteredConsumer);
        QObject::connect(consumer.get(), &wrk::Consumer::endProcessingQuery, this, &QueuingSystemDynGui::queryLeftConsumer);
    }

    const Buffer & buffer = qs_->buffer();

    QObject::connect(&buffer, &wrk::Buffer::queryPutIntoBuffer, this, &QueuingSystemDynGui::queryPutIntoBuffer);    

    QObject::connect(&buffer, &wrk::Buffer::queryRejected, this, &QueuingSystemDynGui::queryRejected);

    QObject::connect(&buffer, &wrk::Buffer::queryLeftBuffer, this, &QueuingSystemDynGui::queryLeftBuffer);       

    QObject::connect(qs_.get(), &wrk::QueuingSystem::modelingFinished, this, &QueuingSystemDynGui::setReady);

    auto signals_ = parser_.jsonSignals();
    args_<< "--input" << jsonFilename_ << "--png" << imageFilename_;

    updatePicture(signals_);

    diagram_.load(imageFilename_);
    diagramLabel_.setPixmap(diagram_);
    scrollArea_.setWidget(&diagramLabel_);
    scrollArea_.setWidgetResizable(true);
    scrollArea_.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    layout_.addWidget(&nextButton_);
    layout_.addWidget(&scrollArea_);
    setLayout(&layout_);

    QObject::connect(&nextButton_, &QPushButton::clicked, &loop_, &QEventLoop::quit);
}

void wrk::QueuingSystemDynGui::runSimulation()
{
    qs_->run();
}

void wrk::QueuingSystemDynGui::closeEvent(QCloseEvent * event)
{
    std::cout << "here\n";
    loop_.exit();
    futureEvents_.clear();
}

void wrk::QueuingSystemDynGui::setReady()
{
    isReady_ = true;
    sortEvents();
    run();
}

bool wrk::QueuingSystemDynGui::isReady() const
{
    return isReady_;
}

void wrk::QueuingSystemDynGui::run()
{
    if (!isReady_)
    {
        return;
    }

    show();
    loop_.exec();

    int size = futureEvents_.size();
    for (int i = 0; i < size; ++i)
    {
        auto event = futureEvents_.front();

        parser_.parse(event);
        auto jsonSignals = parser_.jsonSignals();
        updatePicture(jsonSignals);
        futureEvents_.removeFirst();
        
        loop_.exec();
    }
}

void wrk::QueuingSystemDynGui::queryGenerated(Query query)
{
    futureEvents_.push_back(Event{wrk::TypeOfEvent::QueryGenerated, QTime::currentTime(), query});
}

void wrk::QueuingSystemDynGui::queryPutIntoBuffer(Query query, int index)
{
    futureEvents_.push_back(Event{wrk::TypeOfEvent::QueryPutIntoBuffer, QTime::currentTime(), query, index});
}

void wrk::QueuingSystemDynGui::queryRejected(Query query)
{
    futureEvents_.push_back(Event{wrk::TypeOfEvent::QueryRejected, QTime::currentTime(), query});
}

void wrk::QueuingSystemDynGui::queryLeftBuffer(Query query, int index)
{
    futureEvents_.push_back(Event{wrk::TypeOfEvent::QueryLeftBuffer, QTime::currentTime(), query, index});
}

void wrk::QueuingSystemDynGui::queryEnteredConsumer(Query query, int index)
{
    futureEvents_.push_back(Event {wrk::TypeOfEvent::QueryEnteredConsumer, QTime::currentTime(), query, index});
}

void wrk::QueuingSystemDynGui::queryLeftConsumer(Query query, int random, int index)
{
    futureEvents_.push_back(Event {wrk::TypeOfEvent::QueryLeftConsumer, QTime::currentTime(), query, index});
}

void wrk::QueuingSystemDynGui::updatePicture(const QJsonObject & signals_)
{
    QFile fileJson(jsonFilename_);
    fileJson.open(QFile::WriteOnly);
    fileJson.write(QJsonDocument(signals_).toJson());
    fileJson.close();

    QProcess process;
    process.start(bin_, args_);
    process.waitForFinished(-1);
    
    diagram_.load(imageFilename_);
    diagramLabel_.setPixmap(diagram_);

    repaint();
}

void wrk::QueuingSystemDynGui::sortEvents()
{
    std::sort(
        std::begin(futureEvents_),
        std::end(futureEvents_),
        [] (const Event & lhs, const Event & rhs) {
            if (lhs.time_ < rhs.time_)
            {
                return true;
            }
            else if ((lhs.time_ == rhs.time_) &&
                (static_cast< int >(lhs.type_) < static_cast< int >(rhs.type_)))
            {
                return true;
            }
            else if ((lhs.time_ == rhs.time_) &&
                (lhs.query_.producerId() < rhs.query_.producerId()))
            {
                return true;
            }
            else if ((lhs.time_ == rhs.time_) && //мб лишнее
                (lhs.query_.queryId() < rhs.query_.queryId()))
            {
                return true;
            }
            return false;
        }
    );
}