#ifndef CONSUMERS_HPP
#define CONSUMERS_HPP

#include <QObject>
#include <QVector>

#include <memory>

#include "Consumer.hpp"
#include "Query.hpp"

namespace wrk
{
    class Consumers : public QObject
    {
        Q_OBJECT
    public:
        Consumers(int, int);

        const QVector< std::shared_ptr< Consumer > > & consumers() const;

    public slots:
        void getQueryFromBuffer(); // Для того, чтобы проверить, есть ли свободные consumer
        void receiveQuery(Query query); // для того, чтобы обработать query, в случае, если получен
                                        // сигнал isReadyToReceiveQuery
        void stop();
        void resume();

    signals:
        void isReadyToReceiveQuery();
        void allConsumersAreBusy();
        void lastQueryInConsumers();

    private:
        QVector< std::shared_ptr< Consumer > > consumers_;
    };
}

#endif