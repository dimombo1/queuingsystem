#ifndef QUEUING_SYSTEM_AUTO_GUI_HPP
#define QUEUING_SYSTEM_AUTO_GUI_HPP

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QTableWidgetItem>
#include "QueuingSystem.hpp"

namespace wrk
{
    class QueuingSystemAutoGui : public QWidget
    {
        Q_OBJECT
    public:
        QueuingSystemAutoGui(std::shared_ptr< QueuingSystem >, const SystemSettings &);
        bool isReady() const;
    public slots:
        void processResults();
        void run();
        void setReady();
    signals:
    private:
        std::shared_ptr< QueuingSystem > qs_;
        QTableWidget table_;
        QVector< std::shared_ptr< QTableWidgetItem > > items_;

        QStringList namesOfCharachteristics_;

        bool isReady_;

        void configureTable(const SystemSettings &);
        void configureFormLayout();

        void fillTableWithAverageTimeInSystem();
        void fillTableWithAverageTimeProcessing();
        void fillTableWithAverageTimeAwaitingQuery();
        void fillTableWithVarianceOfTimeAwaitingQuery();
        void fillTableWithVarianceOfTimeProcessing();
        void fillTableWithProbabilityOfRejection();
        void fillTableWithTotalGenerated();
        void fillTableWithEfficiency();
        
        template < typename T >
        void fillTableWithProducerData(const QMap< std::shared_ptr< Producer >, T >&, int);

        void fillTableWithConsumerData(const QMap< std::shared_ptr< Consumer >, double >&, int);
    };

    template < typename T >
    void QueuingSystemAutoGui::fillTableWithProducerData(
        const QMap< std::shared_ptr< Producer >, T > & data, int columnIndex
    )
    {
        auto producers = qs_->producers();
        int i = 0;
        for (auto && producer: producers)
        {
            double avg = data[producer];
            auto item = std::make_shared< QTableWidgetItem >(QString("%1").arg(avg));
            items_.push_back(item);
            table_.setItem(i++, columnIndex, item.get());       
        }
        auto consumers = qs_->consumers();
        while (i < table_.rowCount())
        {
            auto item = std::make_shared< QTableWidgetItem >(QString("-"));
            items_.push_back(item);
            table_.setItem(i++, columnIndex, item.get());     
        }
        table_.repaint();
    }
}

#endif