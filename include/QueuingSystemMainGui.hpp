#ifndef QUEUING_SYSTEM_MAIN_GUI_HPP
#define QUEUING_SYSTEM_MAIN_GUI_HPP

#include <QWidget>
#include <QVector>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QSpinBox>
#include <QPushButton>
#include <QRadioButton>
#include <QLabel>
#include "QueuingSystemAutoGui.hpp"
#include "QueuingSystemDynGui.hpp"

namespace wrk
{
    class QueuingSystemMainGui : public QWidget
    {
    Q_OBJECT
    public:
        QueuingSystemMainGui(const SystemSettings &, const QString & binPath);

    public slots:
        void runSimulation();
    private:
        std::shared_ptr< QueuingSystem > qs_;
        std::shared_ptr< QueuingSystemAutoGui > auto_;
        std::shared_ptr< QueuingSystemDynGui > dyn_;

        QVector< std::shared_ptr< QSpinBox > > spinBoxes_;
        QVector< std::shared_ptr< QLabel > > labels_;
        QVBoxLayout mainLayout_;
        QFormLayout params_;
        QHBoxLayout modes_;
        QRadioButton autoMode_;
        QRadioButton dynMode_;
        QPushButton startButton_;

        SystemSettings settings_;

        QString binPath_;

        void addRow(const QString & name);
        static int numberOfParams();
        void getSettings();
    };
}


#endif