#ifndef CONSUMER_HPP
#define CONSUMER_HPP

#include <QObject>
#include <QVector>
#include <QTimer>

#include <random>

#include "Query.hpp"

namespace wrk
{
  class Consumer: public QObject
  {
    Q_OBJECT

  public:
    Consumer(int, int);
    
    bool isBusy() const;
    int id() const;
    const QVector< Query > & processed() const;
    int totalTimeProcessing() const;
    double efficiency(int) const;

    void setBusy(Query);

  public slots:
     void getQueryFromBuf();
     void receiveQuery(Query);

     void stop();
     void resume();

  signals:
    void startProcessingQuery(Query, int);
    void endProcessingQuery(Query, int, int);
    void isReadyToReceiveQuery();

  private slots:
    void setFree();

  private:
    int id_;
    int totalTimeProcessing_;
    bool isBusy_;
    QVector< Query > processed_; // избыточность, нужно только их общее количество
    QTimer timer_;
    int remainingTime_;

    std::random_device rd_;
    std::mt19937 gen_;
    std::uniform_int_distribution<> distr_;
  };
}

#endif
