#ifndef EVENTS_HPP
#define EVENTS_HPP

#include <QTime>
#include "Query.hpp"

namespace wrk
{

    enum class TypeOfEvent
    {
        QueryGenerated,
        QueryPutIntoBuffer,
        QueryRejected,
        QueryLeftBuffer,
        QueryEnteredConsumer,
        QueryLeftConsumer
    };

    struct Event
    {
        TypeOfEvent type_;
        QTime time_;
        Query query_;
        int index_;
    };
}

#endif