#ifndef QUERY_HPP
#define QUERY_HPP

#include <QTime>

namespace wrk
{
  class Query
  {
  public:
    Query(int, int);
    int producerId() const;
    int queryId() const;

    const QTime & generationTime() const;

    bool operator==(const Query &) const;

  private:
    int producerId_;
    int queryId_;

    QTime generationTime_;
  };

  bool compare(const Query &, const Query &);
}

#endif
