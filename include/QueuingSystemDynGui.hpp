#ifndef QUEUING_SYSTEM_DYN_GUI
#define QUEUING_SYSTEM_DYN_GUI

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QVector>
#include <QEventLoop>
#include <QStringList>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QPixmap>
#include "QueuingSystem.hpp"
#include "Parser.hpp"
#include "Events.hpp"

namespace wrk
{
    class QueuingSystemDynGui : public QWidget
    {
        Q_OBJECT
    public:
        QueuingSystemDynGui(std::shared_ptr< QueuingSystem >, const QString &);
        bool isReady() const;
    public slots:
        void run();
        void runSimulation();
        void setReady();

        void closeEvent(QCloseEvent *) override;

        void queryGenerated(Query);
        void queryPutIntoBuffer(Query, int);
        void queryRejected(Query);
        void queryLeftBuffer(Query, int);
        void queryEnteredConsumer(Query, int);
        void queryLeftConsumer(Query, int, int);
    private:
        std::shared_ptr< QueuingSystem > qs_;
        Parser parser_;
        QPushButton nextButton_;
        QPixmap diagram_;
        QVBoxLayout layout_;
        QScrollArea scrollArea_;

        QString bin_;
        QString jsonFilename_;
        QString imageFilename_;
        QLabel  diagramLabel_;
        QStringList args_;

        QEventLoop loop_;

        QVector< Event > futureEvents_;

        bool isReady_;

        void updatePicture(const QJsonObject&);

        void sortEvents();
    };
}

#endif