#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <QObject>
#include <QList>
#include "Query.hpp"

namespace wrk
{
  class Buffer : public QObject
  {
    Q_OBJECT

  public:
    Buffer(int);

    int capacity() const;
    const QList< Query > & queries() const;

  public slots:
    void receiveQueryFromProducer(Query);
    void sendQueryToConsumer();

  signals:
    void queryPutIntoBuffer(Query, int);
    void queryLeftBuffer(Query, int);
    void queryRejected(Query);
    void querySentToConsumer(Query);

  private:
    int capacity_;
    QList< Query > queries_;

    int getIndex(QList< Query >::iterator);
  };
}

#endif
