#ifndef QUEUING_SYSTEM_HPP
#define QUEUING_SYSTEM_HPP

#include <QObject>
#include <QVector>
#include <QMap>
#include <QTime>

#include <memory>

#include "Consumers.hpp"
#include "Producer.hpp"
#include "Buffer.hpp"

namespace wrk
{

    struct SystemSettings;

    class QueuingSystem : public QObject
    {
        Q_OBJECT
    public:
        using Producers = QVector< std::shared_ptr< Producer > >;

        QueuingSystem(const SystemSettings &);  

        QMap< std::shared_ptr< Producer >, double > averageTimeInSystem() const;
        QMap< std::shared_ptr< Producer >, double > averageTimeProcessing() const;
        QMap< std::shared_ptr< Producer >, double > averageTimeAwaitingQuery() const;
        QMap< std::shared_ptr< Producer >, double > probabilityOfRejection() const;
        QMap< std::shared_ptr< Producer >, double > varianceTimeAwaitingQuery() const;
        QMap< std::shared_ptr< Producer >, double > varianceTimeProcessing() const;
        QMap< std::shared_ptr< Producer >, int > totalGenerated() const;
        QMap< std::shared_ptr< Consumer >, double > efficiency() const;

        int numberOfCharachteristics() const;
        int bufSize() const;

        const Producers & producers() const;
        const QVector< std::shared_ptr< Consumer > > & consumers() const;
        const Buffer & buffer() const;

    public slots:
        void queryLeftConsumer(Query, int, int);
        void run();
        void increaseNumberOfRejected(Query);
        void stop();
        void resume();
    signals:
        void modelingFinished();

    private:
        Producers producers_;
        Buffer buffer_;
        Consumers consumers_;

        int totalQueries_;
        int maxQueries_;
        int totalRejected_;

        QMap< std::shared_ptr< Producer >, int > totalTimeInSystem_;
        QMap< std::shared_ptr< Producer >, int > totalTimeProcessing_; // избыточность
        QMap< std::shared_ptr< Producer >, QMap< int, int > > deltasProcessing_;

        QTime startOfSimulation_;

        QMap< std::shared_ptr< Producer >, QMap< int, double > > distributionProcessing() const;
    };


    struct SystemSettings
    {
      int numberOfProducers_;
      int numberOfConsumers_;
      int bufferSize_;
      int poissonMean_;
      int totalQueries_;
      int processingMean_;
    };
}

#endif