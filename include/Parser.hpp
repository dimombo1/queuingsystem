#ifndef PARSER_HPP
#define PARSER_HPP

#include <QJsonArray>
#include <QJsonObject>
#include <QTime>
#include <memory>
#include "QueuingSystem.hpp"
#include "Events.hpp"

namespace wrk
{
    class Parser
    {
    public:
        Parser(const std::shared_ptr< QueuingSystem >, const QTime &);
        void parse(const Event &);
        //добавить проверку на то, та ли это система или другая
        void queryPutIntoBuffer(const Query &, const QTime &, int);
        void queryRejected(const Query &, const QTime &);
        void queryLeftBuffer(const Query &, const QTime &, int);
        void queryEnteredConsumer(const Query &, const QTime &, int);
        void queryLeftConsumer(const Query &, const QTime &, int);
        const QJsonObject & jsonSignals() const;

    private:
        QJsonArray signals_;
        QJsonObject obj_;
        QTime prev_;
        int numberOfProducers_;
        int numberOfConsumers_;

        int divide_;

        void queryGenerated(const Query &, const QTime &);

        void init(const std::shared_ptr< QueuingSystem >);
        void addNDotsToAllElements(int); // добавляет по 2 точке ко всем кроме
        //прибора с заданным id
        void reconfigureBuffer(int);

        void updateSignals(const QJsonArray &, const QString &, const QString &, int);
    };
}

#endif