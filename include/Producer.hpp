#ifndef PRODUCER_HPP
#define PRODUCER_HPP

#include <QObject>
#include <QTimer>
#include <QMap>
#include <random>
#include "Query.hpp"

namespace wrk
{
  class Producer : public QObject
  {
    Q_OBJECT

  public:
    Producer(int id, int maxQueries, int poissonMean = 500);
    int id() const;
    int generatedTotal() const;
    int rejectedTotal() const;

    double probabilityOfRejection() const;
    double averageTimeAwaitingQuery() const;
    double varianceOfTimeAwaitingQuery(int) const;
    
  public slots:
    void run();
    void stop();
    void resume();
    void increaseNumberOfRejected();
    void generateQuery();


  signals:
    void queryGenerated(Query);
    void tooMuchQueries();
    
  private:
    int id_;
    int generatedTotal_;
    int rejectedTotal_;
    int poissonMean_;
    int totalTimeAwaitingForQuery_; // избыточно

    int maxQueries_;

    int remainingTime_;

    QTimer timer_;
    std::default_random_engine generator_;
    std::poisson_distribution< int > distr_;
    QMap< int, int > deltas_;

    QMap< int, double > distribution() const;
  };
}

#endif
